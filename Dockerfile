FROM ruby:2.3.1-alpine

WORKDIR /app

COPY . .

RUN apk update \
    && apk add linux-headers \
      build-base \
      tzdata \
      libxml2-dev \
      libxslt-dev \
      libc-dev \
      ruby-dev \
      xz-dev \
      nodejs \
      sqlite-dev \
      postgresql-dev \
      sqlite-dev \
      xz-dev

RUN bundle config build.nokogiri --use-system-libraries \
    && bundle install

# BE THOU STATIC
ENV BOOL_API_URL=http://boolio.nav.engineering/booleans

RUN /app/bin/rake assets:precompile

EXPOSE 5000/tcp

CMD [ "/bin/sh", "-c", "/app/docker-entrypoint.sh" ]
